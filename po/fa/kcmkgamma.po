# translation of kgamma.po to Persian
# Nasim Daniarzadeh <daniarzadeh@itland.ir>, 2006.
# MaryamSadat Razavi <razavi@itland.ir>, 2006.
# Tahereh Dadkhahfar <dadkhahfar@itland.ir>, 2006.
# Nazanin Kazemi <kazemi@itland.ir>, 2007.
msgid ""
msgstr ""
"Project-Id-Version: kgamma\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-12 00:46+0000\n"
"PO-Revision-Date: 2007-06-24 11:42+0330\n"
"Last-Translator: Nazanin Kazemi <kazemi@itland.ir>\n"
"Language-Team: Persian <kde-i18n-fa@kde.org>\n"
"Language: fa\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: kgamma.cpp:132
#, kde-format
msgid "&Select test picture:"
msgstr "&برگزیدن عکس آزمون:‌"

#: kgamma.cpp:137
#, kde-format
msgid "Gray Scale"
msgstr "مقیاس خاکستری"

#: kgamma.cpp:137
#, kde-format
msgid "RGB Scale"
msgstr "مقیاس RGB"

#: kgamma.cpp:137
#, kde-format
msgid "CMY Scale"
msgstr "مقیاس CMY"

#: kgamma.cpp:137
#, kde-format
msgid "Dark Gray"
msgstr "خاکستری تیره"

#: kgamma.cpp:137
#, kde-format
msgid "Mid Gray"
msgstr "خاکستری متوسط"

#: kgamma.cpp:137
#, kde-format
msgid "Light Gray"
msgstr "خاکستری روشن"

#: kgamma.cpp:185
#, kde-format
msgid "Gamma:"
msgstr "گاما:"

#: kgamma.cpp:188
#, kde-format
msgid "Red:"
msgstr "قرمز:"

#: kgamma.cpp:191
#, kde-format
msgid "Green:"
msgstr "سبز:"

#: kgamma.cpp:194
#, kde-format
msgid "Blue:"
msgstr "آبی:"

#: kgamma.cpp:236
#, kde-format
msgid "Save settings system wide"
msgstr "ذخیره تنظیمات کل سیستم"

#: kgamma.cpp:240
#, kde-format
msgid "Sync screens"
msgstr "پرده‌های همگام‌سازی"

#: kgamma.cpp:248
#, kde-format
msgid "Screen %1"
msgstr "پرده %1"

#: kgamma.cpp:266
#, kde-format
msgid "Gamma correction is not supported by your graphics hardware or driver."
msgstr "اصلاح گاما توسط سخت‌افزار یا گرداننده نگاره‌سازی شما پشتیبانی نمی‌شود."

#: kgamma.cpp:604
#, kde-format
msgid ""
"<h1>Monitor Gamma</h1> This is a tool for changing monitor gamma correction. "
"Use the four sliders to define the gamma correction either as a single "
"value, or separately for the red, green and blue components. You may need to "
"correct the brightness and contrast settings of your monitor for good "
"results. The test images help you to find proper settings.<br> You can save "
"them system-wide to XF86Config (root access is required for that) or to your "
"own KDE settings. On multi head systems you can correct the gamma values "
"separately for all screens."
msgstr ""
"<h1>گامای نمایشگر</h1> ابزاری برای تغییر اصلاح گامای نمایشگر می‌باشد. برای "
"تعریف اصلاح گاما به صورت یک مقدار تک یا به طور جداگانه برای مؤلفه‌های قرمز، "
"سبز و آبی، از چهار لغزان استفاده کنید. ممکن است لازم باشد که برای نتایج "
"بهتر، تنظیمات براقی و سایه روشنی نمایشگرتان را اصلاح کنید. تصاویر آزمون در "
"یافتن تنظیمات مناسب، به شما کمک می‌کنند.<br> می‌توانید آنها را به صورت کل "
"سیستمی در XF86Config )دستیابی به ریشه برای آن لازم است( یا در تنظیمات KDE "
"خود ذخیره کنید. در مورد سیستمهای چند سری، می‌توانید مقادیر گاما را به طور "
"جداگانه برای تمام پرده‌ها اصلاح کنید."
