# translation of kgamma.po to Finnish
# Copyright (C).
# Teemu Rytilahti <teemu.rytilahti@kde-fi.org>, 2003, 2008.
# Ilpo Kantonen <ilpo@iki.fi>, 2005.
# Lasse Liehu <lasse.liehu@gmail.com>, 2013, 2015.
#
# KDE Finnish translation sprint participants:
# Author: Lliehu
msgid ""
msgstr ""
"Project-Id-Version: kcmkgamma\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-06-12 00:46+0000\n"
"PO-Revision-Date: 2015-08-07 00:10+0200\n"
"Last-Translator: Lasse Liehu <lasse.liehu@gmail.com>\n"
"Language-Team: Finnish <kde-i18n-doc@kde.org>\n"
"Language: fi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-POT-Import-Date: 2012-12-01 22:24:10+0000\n"
"X-Generator: Lokalize 2.0\n"

#: kgamma.cpp:132
#, kde-format
msgid "&Select test picture:"
msgstr "&Valitse testikuva:"

#: kgamma.cpp:137
#, kde-format
msgid "Gray Scale"
msgstr "Harmaasävy"

#: kgamma.cpp:137
#, kde-format
msgid "RGB Scale"
msgstr "RGB-sävy"

#: kgamma.cpp:137
#, kde-format
msgid "CMY Scale"
msgstr "CMY-sävy"

#: kgamma.cpp:137
#, kde-format
msgid "Dark Gray"
msgstr "Tummanharmaa"

#: kgamma.cpp:137
#, kde-format
msgid "Mid Gray"
msgstr "Keskiharmaa"

#: kgamma.cpp:137
#, kde-format
msgid "Light Gray"
msgstr "Vaaleanharmaa"

#: kgamma.cpp:185
#, kde-format
msgid "Gamma:"
msgstr "Gamma:"

#: kgamma.cpp:188
#, kde-format
msgid "Red:"
msgstr "Punainen:"

#: kgamma.cpp:191
#, kde-format
msgid "Green:"
msgstr "Vihreä:"

#: kgamma.cpp:194
#, kde-format
msgid "Blue:"
msgstr "Sininen:"

#: kgamma.cpp:236
#, kde-format
msgid "Save settings system wide"
msgstr "Tallenna asetukset järjestelmänlaajuisiksi"

#: kgamma.cpp:240
#, kde-format
msgid "Sync screens"
msgstr "Tahdista näytöt"

#: kgamma.cpp:248
#, kde-format
msgid "Screen %1"
msgstr "Näyttö %1"

#: kgamma.cpp:266
#, kde-format
msgid "Gamma correction is not supported by your graphics hardware or driver."
msgstr "Näytönohjain tai ajuri ei tue gammakorjausta."

#: kgamma.cpp:604
#, kde-format
msgid ""
"<h1>Monitor Gamma</h1> This is a tool for changing monitor gamma correction. "
"Use the four sliders to define the gamma correction either as a single "
"value, or separately for the red, green and blue components. You may need to "
"correct the brightness and contrast settings of your monitor for good "
"results. The test images help you to find proper settings.<br> You can save "
"them system-wide to XF86Config (root access is required for that) or to your "
"own KDE settings. On multi head systems you can correct the gamma values "
"separately for all screens."
msgstr ""
"<h1>Näytön gamma</h1> Tällä työkalulla voi muuttaa näytön gammakorjausta. "
"Käytä neljää liukusäädintä määrittääksesi gammakorjauksen yhtenä arvona tai "
"erikseen punaiselle, vihreälle ja siniselle komponentille. Kirkkautta ja "
"kontrastia tarvitsee ehkä korjata hyvän tuloksen saavuttamiseksi. Testikuva "
"auttaa löytämään sopivat asetukset.<br> Voit tallentaa asetukset "
"järjestelmänlaajuisiksi XF86Config-ohjelmalla (ohjelma tarvitsee ylläpitäjän "
"oikeudet) tai voit tallentaa omiin KDE-asetuksiisi. Usean näytön "
"järjestelmissä kunkin näytön gamma-arvon voi korjata erikseen."
